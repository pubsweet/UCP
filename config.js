module.exports = {
  secret: 'EXAMPLEDONTUSE',
  API_ENDPOINT: '/api',
  editor: '../registry/Substance/Writer.jsx',
  // editor: '../registry/Quill/Editor.jsx',
  reader: '../registry/Substance/Reader.jsx',
  // reader: '../registry/Quill/Reader.jsx'
  theme: 'pepper'
}
